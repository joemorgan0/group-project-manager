const mysql = require('mysql');
const express = require('express');
const jwt = require('jsonwebtoken');
var app = express();
var cors = require('cors')
const bodyparser = require('body-parser');
app.use(bodyparser.json({limit: '20mb'}));
app.use(bodyparser.urlencoded({limit: '20mb', extended: true})); // Limit file size

const bcrypt = require('bcrypt-nodejs');

app.use(cors()); // Enable Cross Origin

var mysqlConnection = mysql.createConnection( {
	host:'localhost',	
	user:'root',	
	password:'',	
	database:'project',
	multipleStatements: true	
});

mysqlConnection.connect((err)=>{
	if(!err)
		console.log('DB connected successfully');
	 else 
		console.log('DB failed to connect \n Error :'+ JSON.stringify(err));
});

app.listen(3000,()=>console.log('Express Server is running at port no: 3000'));

// User methods

// Add User
app.route('/users').post((req,res)=>{
	var salt = bcrypt.genSaltSync(10);
	var hash = bcrypt.hashSync(req.body.password, salt);
	mysqlConnection.query("INSERT INTO user (`username`, `password`, `email`, `user_type`) VALUES ('"+req.body.username+"', '"+hash+"', '"+req.body.email+"', '"+req.body.user_type+"')",(err, rows, fields)=>{
		if(!err) {
			res.send(rows);
		}
		else
			console.log(err)
	})	
});

// Login 
app.route('/login').post((req,res)=>{
  var email = req.body.email;
  mysqlConnection.query('SELECT * FROM user WHERE email = ?',[email], function (err, rows, fields) {
  if (rows.length < 1) {
	res.send({
		"code": 404,
		"error": "Invalid Email"
	});
  }else {
    if(rows.length > 0){
    	var salt = bcrypt.genSaltSync(10);
	    // Hash and Compare passwords if email is valid
	    bcrypt.compare(req.body.password, rows[0].password, function(err, result) {
	        if (err) { console.log('Invalid Email or Password provided') }
	        else {
	        	if(result == true) {
		        // Successful login if email and password match
			  			let payload = { user_id: rows[0].user_id, username: rows[0].username, email: rows[0].email, user_type: rows[0].user_type }
							let token = jwt.sign(payload, 'secretkey');
			        res.send({token})
		      	} else {
		      		res.send({
		      			"code": 204,
		      			"error": "Invalid Password"
		      		});
		      	}
	        }
	    });
		 }
		}
	});
});


// Get all users
app.route('/users').get((req,res)=>{
	mysqlConnection.query('SELECT * FROM user',(err, rows, fields)=>{
		if(!err)
			res.send(rows);
		else
			console.log(err)
	})	
});

// Get user
app.route('/users/:id').get((req,res)=>{
	mysqlConnection.query('SELECT * FROM user WHERE user_id = ?',[req.params.id],(err, rows, fields)=>{
		if(!err)
			res.send(rows);
		else
			console.log(err)
	})	
});

// Get user by project_id
app.route('/users/project/:projectid').get((req,res)=>{
	mysqlConnection.query('SELECT * FROM user WHERE project_id = ?',[req.params.projectid],(err, rows, fields)=>{
		if(!err)
			res.send(rows);
		else
			console.log(err)
	})	
});

// Delete user
app.route('/users/:id').delete((req,res)=>{
	mysqlConnection.query('DELETE FROM user WHERE user_id = ?',[req.params.id],(err, rows, fields)=>{
		if(!err)
			res.send('User Deleted');
		else
			console.log(err)
	})	
});

// Group methods

// Get all groups
app.route('/groups').get((req,res)=>{
	mysqlConnection.query('SELECT * FROM `group`',(err, rows, fields)=>{
		if(!err)
			res.send(rows);
		else
			console.log(err)
	})	
});

// Get group
app.route('/groups/:id').get((req,res)=>{
	mysqlConnection.query('SELECT * FROM `group` WHERE group_id = ?',[req.params.id],(err, rows, fields)=>{
		if(!err)
			res.send(rows);
		else
			console.log(err)
	})	
});

// Delete group
app.route('/groups/:id').delete((req,res)=>{
	mysqlConnection.query('DELETE FROM `group` WHERE group_id = ?',[req.params.id],(err, rows, fields)=>{
		if(!err)
			res.send('Group Deleted');
		else
			console.log(err)
	})	
});


// Get all projects
app.route('/projects').get((req,res)=>{
	mysqlConnection.query('SELECT * FROM projects',(err, rows, fields)=>{
		if(!err)
			res.send(rows);
		else
			console.log(err)
	})	
});

// Get user projects 
app.route('/projects/user/:id').get((req,res)=>{
	mysqlConnection.query('SELECT * FROM project_members WHERE user_id = ?',[req.params.id],(err, rows, fields)=>{
		if(!err)
			res.send(rows);
		else
			console.log(err)
	})	
});

// Get project
app.route('/projects/:id').get((req,res)=>{
	mysqlConnection.query('SELECT * FROM project WHERE project_id = ?',[req.params.id],(err, rows, fields)=>{
		if(!err)
			res.send(rows);
		else
			console.log(err)
	})	
});

// Add project
// app.route('/projects').post((req,res)=>{
// 	mysqlConnection.query("INSERT INTO project (`project_name`, `project_start`, `project_end`, `user_id`, `description`) VALUES ('"+req.body.project_name+"', '"+req.body.project_start+"', '"+req.body.project_end+"', '"+req.body.user_id+"', '"+req.body.description+"')",(err, rows, fields)=>{
// 		if(!err)
// 			res.send(rows);
// 		else
// 			console.log(err)
// 	})	
// });

// Add project
app.route('/projects').post((req,res)=>{
	mysqlConnection.query("CALL create_project ('"+req.body.project_name+"', '"+req.body.user_id+"', '"+req.body.project_start+"', '"+req.body.project_end+"', '"+req.body.description+"')",(err, rows, fields)=>{
		if(!err)
			res.send(rows);
		else
			console.log(err)
	})	
});



// update project
app.route('/projects/:id').put((req,res)=>{
	mysqlConnection.query("UPDATE project SET `project_name` = '"+req.body.project_name+"', `description` = '"+req.body.description+"' WHERE (`project_id` = ?)",[req.params.id],(err, rows, fields)=>{
		if(!err)
			res.send(rows);
		else
			console.log(err)
	})	
});

// Delete project
app.route('/projects/:id').delete((req,res)=>{
	mysqlConnection.query("CALL delete_project ('"+[req.params.id]+"')",(err, rows, fields)=>{
		if(!err)
			res.send('Project Deleted');
		else
			console.log(err)
	})	
});


// Get project members 
app.route('/projectmembers/:id').get((req,res)=>{
	mysqlConnection.query('SELECT * FROM project_members WHERE project_id = ?',[req.params.id],(err, rows, fields)=>{
		if(!err)
			res.send(rows);
		else
			console.log(err)
	})	
});

// Get project members user details
app.route('/projectmembers/user/:id').get((req,res)=>{
	mysqlConnection.query('SELECT * FROM project_members_users WHERE project_id = ?',[req.params.id],(err, rows, fields)=>{
		if(!err)
			res.send(rows);
		else
			console.log(err)
	})	
});

// Add project member
app.route('/projectmembers').post((req,res)=>{
	mysqlConnection.query("INSERT INTO project_member (`project_id`, `user_id`) VALUES ('"+req.body.project_id+"', '"+req.body.user_id+"')",(err, rows, fields)=>{
		if(!err)
			res.send(rows);
		else
			console.log(err)
	})	
});

// Delete project member
app.route('/projectmembers/:id/:memberid/:userid').delete((req,res)=>{
	mysqlConnection.query("CALL delete_project_member ('"+[req.params.id]+"', '"+[req.params.memberid]+"', '"+[req.params.userid]+"')",(err, rows, fields)=>{
		if(!err)
			res.send(rows);
		else
			console.log(err)
	})	
});

// Get all tasks
app.route('/alltasks/:id').get((req,res)=>{
	mysqlConnection.query('SELECT * FROM tasks WHERE project_id = ?',[req.params.id],(err, rows, fields)=>{
		if(!err)
			res.send(rows);
		else
			console.log(err)
	})	
});

// Get task
app.route('/tasks/:id').get((req,res)=>{
	mysqlConnection.query('SELECT * FROM tasks WHERE task_id = ?',[req.params.id],(err, rows, fields)=>{
		if(!err)
			res.send(rows);
		else
			console.log(err)
	})	
});

// Add task
app.route('/tasks').post((req,res)=>{
	mysqlConnection.query("INSERT INTO task (`title`, `user_id`, `created_id`, `start_date`, `end_date`, `project_id`, `description`, `status`, `priority`) VALUES ('"+req.body.title+"', '"+req.body.user_id+"', '"+req.body.created_id+"', '"+req.body.start_date+"', '"+req.body.end_date+"', '"+req.body.project_id+"', '"+req.body.description+"', '"+req.body.status+"', '"+req.body.priority+"')",(err, rows, fields)=>{
		if(!err)
			res.send(rows);
		else
			console.log(err)
	})	
});

// Update task status to completed
app.route('/completedtasks').put((req,res)=>{
	mysqlConnection.query("UPDATE task SET `status` = '"+req.body.status+"', `completed` = CURRENT_TIMESTAMP WHERE (`task_id` = '"+req.body.task_id+"')",(err, rows, fields)=>{
		if(!err)
			res.send(rows);
		else
			console.log(err)
	})	
});

// update task status
app.route('/tasks').put((req,res)=>{
	mysqlConnection.query("UPDATE task SET `status` = '"+req.body.status+"', `completed` = NULL WHERE (`task_id` = '"+req.body.task_id+"')",(err, rows, fields)=>{
		if(!err)
			res.send(rows);
		else
			console.log(err)
	})	
});

// update task details
app.route('/tasks/:id').put((req,res)=>{
	mysqlConnection.query("UPDATE task SET `user_id` = '"+req.body.user_id+"', `priority` = '"+req.body.priority+"', `description` = '"+req.body.description+"' WHERE (`task_id` = ?)",[req.params.id],(err, rows, fields)=>{
		if(!err)
			res.send(rows);
		else
			console.log(err)
	})	
});

// Delete task
app.route('/tasks/:id/:taskid/').delete((req,res)=>{
	mysqlConnection.query("CALL delete_task ('"+[req.params.id]+"', '"+[req.params.taskid]+"')",(err, rows, fields)=>{
		if(!err)
			res.send(rows);
		else
			console.log(err)
	})	
});


// Get all task comments
app.route('/alltaskcomments/:id').get((req,res)=>{
	mysqlConnection.query('SELECT * FROM task_comments WHERE project_id = ?',[req.params.id],(err, rows, fields)=>{
		if(!err)
			res.send(rows);
		else
			console.log(err)
	})	
});

// Get selected task comments
app.route('/taskcomments/:id').get((req,res)=>{
	mysqlConnection.query('SELECT * FROM task_comments WHERE task_id = ?',[req.params.id],(err, rows, fields)=>{
		if(!err)
			res.send(rows);
		else
			console.log(err)
	})	
});

// Post task comments
app.route('/taskcomments').post((req,res)=>{
	mysqlConnection.query("INSERT INTO task_comment (`task_id`, `user_id`, `project_id`, `comment`) VALUES ('"+req.body.task_id+"', '"+req.body.user_id+"', '"+req.body.project_id+"', '"+req.body.comment+"')",(err, rows, fields)=>{
		if(!err)
			res.send(rows);
		else
			console.log(err)
	})	
});

// Get comment count
app.route('/taskcommentscount/:id').get((req,res)=>{
	mysqlConnection.query('SELECT * FROM comment_count WHERE project_id = ?',[req.params.id],(err, rows, fields)=>{
		if(!err)
			res.send(rows);
		else
			console.log(err)
	})	
});

// Delete selected task comment
app.route('/taskcomments/:id/:commentid').delete((req,res)=>{
	mysqlConnection.query('DELETE FROM task_comment WHERE comment_id = ?',[req.params.commentid],(err, rows, fields)=>{
		if(!err)
			res.send(rows);
		else
			console.log(err)
	})	
});

// Get all file attachments
app.route('/taskattachments').get((req,res)=>{
	mysqlConnection.query('SELECT * FROM task_attachments',(err, rows, fields)=>{
		if(!err)
			res.send(rows);
		else
			console.log(err)
	})	
});


// Get selected task attachments
app.route('/taskattachments/:id').get((req,res)=>{
	mysqlConnection.query('SELECT * FROM task_attachments WHERE task_id = ?',[req.params.id],(err, rows, fields)=>{
		if(!err)
			res.send(rows);
		else
			console.log(err)
	})	
});

// Post task attachment
app.route('/taskattachments').post((req,res)=>{
	mysqlConnection.query("INSERT INTO task_attachment (`task_id`, `user_id`, `project_id`, `attachment_name`, `attachment`) VALUES ('"+req.body.task_id+"', '"+req.body.user_id+"', '"+req.body.project_id+"', '"+req.body.attachment_name+"', '"+req.body.attachment+"')",(err, rows, fields)=>{
		if(!err)
			res.send(rows);
		else
			console.log(err)
	})	
});

// Delete selected task attachment
app.route('/taskattachments/:id/:attachmentid').delete((req,res)=>{
	mysqlConnection.query('DELETE FROM task_attachment WHERE attachment_id = ?',[req.params.attachmentid],(err, rows, fields)=>{
		if(!err)
			res.send(rows);
		else
			console.log(err)
	})	
});

// Get attachment count
app.route('/taskattachmentscount/:id').get((req,res)=>{
	mysqlConnection.query('SELECT * FROM attachment_count WHERE project_id = ?',[req.params.id],(err, rows, fields)=>{
		if(!err)
			res.send(rows);
		else
			console.log(err)
	})	
});


// Report Methods

// Get Tasks By Status
app.route('/tasksbystatus/:id').get((req,res)=>{
	mysqlConnection.query('SELECT * FROM tasks_by_status WHERE project_id = ?',[req.params.id],(err, rows, fields)=>{
		if(!err)
			res.send(rows);
		else
			console.log(err)
	})	
});

// Get Completed Tasks By User
app.route('/completedtasksbyuser/:id').get((req,res)=>{
	mysqlConnection.query('SELECT * FROM completed_tasks_by_user WHERE project_id = ? ORDER BY count DESC',[req.params.id],(err, rows, fields)=>{
		if(!err)
			res.send(rows);
		else
			console.log(err)
	})	
});

// Get Tasks Completed On Time
app.route('/taskscompletedontime/:id').get((req,res)=>{
	mysqlConnection.query('SELECT * FROM tasks_completed_on_time WHERE project_id = ? ORDER BY count DESC',[req.params.id],(err, rows, fields)=>{
		if(!err)
			res.send(rows);
		else
			console.log(err)
	})	
});

// Get Tasks Completed Late
app.route('/taskscompletedlate/:id').get((req,res)=>{
	mysqlConnection.query('SELECT * FROM tasks_completed_late WHERE project_id = ? ORDER BY count DESC',[req.params.id],(err, rows, fields)=>{
		if(!err)
			res.send(rows);
		else
			console.log(err)
	})	
});